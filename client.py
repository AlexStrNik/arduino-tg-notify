import asyncio
from bleak import discover, BleakClient
from pyrogram import Client, Filters
from transliterate import translit, detect_language

async def run(loop):
    print("Discovering. Please wait...")
    print()

    devices = await discover()
    for i, device in enumerate(devices):
        print(f"[{i}]: {device.name} ({device.address})")

    selected = int(input("Please select device: "))
    print()

    print("Connecting. Please wait...")
    print()

    async with BleakClient(devices[selected].address, loop) as client:
        if await client.connect():
            print("Connected")
        else:
            print("Connection failed. Exit...")
            return

        svcs = await client.get_services()
        services = list(svcs.services.values())
        
        for i, service in enumerate(services):
            print(f"[{i}]: {service.description} ({service.uuid})")
        
        selected = int(input("Please select service: "))
        print()

        service = services[selected]

        for i, characteristic in enumerate(service.characteristics):
            print(f"[{i}]: {characteristic.description} ({characteristic.uuid})")

        selected = int(input("Please select characteristic: "))
        print()

        characteristic = service.characteristics[selected]

        app = Client(
            "tg-notify",
        )

        @app.on_message(Filters.incoming)
        async def notify(_, message):
            if message.text == None:
                return

            text = message.text[:16]
            try:
                text = translit(text, reversed='True')
            except:
                pass

            chat = ""

            if message.chat.type == "private":
                chat = message.chat.first_name

                if message.chat.last_name != None:
                    chat += " " +message.chat.last_name
            elif message.chat.type == "bot":
                chat = message.chat.first_name
            else:
                chat = message.chat.title
            
            chat = chat[:16]
            try:
                chat = translit(chat, reversed='True')
            except:
                pass

            sendString = f"{chat}\r{text}".encode('ascii', 'replace')

            while len(sendString) > 0:
                part = sendString[:10]
                sendString = sendString[10:]
                await client.write_gatt_char(characteristic.uuid, part)

        await app.start()
        await Client.idle()

loop = asyncio.get_event_loop()
loop.run_until_complete(run(loop))