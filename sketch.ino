#include <SoftwareSerial.h>
#include <LiquidCrystal_I2C.h>

#define K 8
#define BT Serial3

LiquidCrystal_I2C lcd(0x27, 16, 2);

void debug()
{
  while (BT.available())
  {
    Serial.write(BT.read());
  }
  Serial.println();
}

void setup()
{
  lcd.init();
  lcd.backlight();
  // lcd.autoscroll();

  lcd.print("Setup in proccess");
  lcd.setCursor(0, 1);
  lcd.print("Please wait ...");
  lcd.setCursor(0, 0);

  Serial.begin(9600);
  BT.begin(9600);

  digitalWrite(K, HIGH);
  delay(100);
  digitalWrite(K, LOW);

  BT.write("AT+RENEW");
  delay(500);
  debug();

  BT.write("AT+MODE0");
  delay(500);
  debug();

  BT.write("AT+NAMETgNotify");
  delay(500);
  debug();

  BT.write("AT+TYPE0");
  delay(500);
  debug();

  BT.write("AT+PASS123456");
  delay(500);
  debug();

  BT.write("AT+RESET");
  delay(500);
  debug();

  lcd.clear();
  lcd.print("Setup complete");
  lcd.setCursor(0, 1);
}

void loop()
{
  String string = BT.readStringUntil('\n');

  if (string.length() == 0)
  {
    return;
  }

  if (string == "OK+CONN" || string == "OK+LOST")
  {
    return;
  }

  String chat, text;

  for (int i = 0; i < string.length(); i++)
  {
    if (string.substring(i, i + 1) == "\r")
    {
      chat = string.substring(0, i);
      text = string.substring(i + 1);
      break;
    }
  }

  lcd.clear();
  lcd.print(chat);
  lcd.setCursor(0, 1);
  lcd.print(text);
}
